v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 560 -280 560 -260 {
lab=VSS}
N 560 -180 560 -160 {
lab=VDD}
N 480 -140 500 -140 {
lab=B}
N 480 -310 500 -310 {
lab=A}
N 560 -350 560 -330 {
lab=VDD}
N 560 -370 560 -350 {
lab=VDD}
N 560 -200 560 -180 {
lab=VDD}
N 560 -260 560 -240 {
lab=VSS}
N 560 -240 560 -230 {
lab=VSS}
N 540 -280 540 -230 {
lab=EN}
N 540 -110 540 -70 {
lab=ENB}
N 540 -70 540 -60 {
lab=ENB}
N 560 -110 560 -60 {
lab=VSS}
N 580 -310 660 -310 {
lab=OUT}
N 660 -310 660 -140 {
lab=OUT}
N 580 -140 660 -140 {
lab=OUT}
N 170 -310 190 -310 {
lab=CNT}
N 220 -290 220 -270 {
lab=VSS}
N 220 -370 220 -330 {
lab=VDD}
N 340 -290 340 -270 {
lab=VSS}
N 340 -370 340 -330 {
lab=VDD}
N 370 -310 390 -310 {
lab=EN}
N 250 -310 310 -310 {
lab=ENB}
C {devices/iopin.sym} 90 -330 2 0 {name=p1 lab=CNT}
C {devices/iopin.sym} 90 -310 2 0 {name=p2 lab=A}
C {devices/iopin.sym} 90 -290 2 0 {name=p3 lab=B}
C {devices/iopin.sym} 90 -270 2 0 {name=p4 lab=OUT}
C {devices/iopin.sym} 90 -220 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 90 -200 2 0 {name=p6 lab=VSS}
C {devices/lab_pin.sym} 480 -310 0 0 {name=l8 sig_type=std_logic lab=A}
C {devices/lab_pin.sym} 480 -140 0 0 {name=l9 sig_type=std_logic lab=B}
C {devices/lab_pin.sym} 560 -200 2 0 {name=l10 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 560 -230 2 0 {name=l11 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 560 -370 2 0 {name=l12 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 560 -60 2 0 {name=l13 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 540 -60 0 0 {name=l15 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 540 -230 0 0 {name=l16 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 660 -220 2 0 {name=l17 sig_type=std_logic lab=OUT}
C {nmos_switch_gen/nmos_switch_templates_xschem/nmos_switch/nmos_switch.sym} 400 -210 0 0 {name=SW1
spiceprefix=X
}
C {nmos_switch_gen/nmos_switch_templates_xschem/nmos_switch/nmos_switch.sym} 400 -40 0 0 {name=SW2
spiceprefix=X
}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 200 -290 0 0 {name=inv1

}
C {devices/lab_pin.sym} 170 -310 0 0 {name=p7 sig_type=std_logic lab=CNT}
C {devices/lab_pin.sym} 220 -270 0 0 {name=p8 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 220 -370 2 0 {name=p9 sig_type=std_logic lab=VDD}
C {inverter2_gen/inverter2_templates_xschem/inverter2/inverter2.sym} 320 -290 0 0 {name=inv2

}
C {devices/lab_pin.sym} 340 -270 0 0 {name=p10 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 340 -370 2 0 {name=p11 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 390 -310 2 0 {name=p12 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 280 -310 3 0 {name=p13 sig_type=std_logic lab=ENB}
