#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from inverter2_gen.params import inverter2_layout_params
from nmos_switch_gen.params import nmos_switch_layout_params


@dataclass
class mux_sw_layout_params(LayoutParamsBase):
    """
    Parameter class for mux_sw_gen

    Args:
    ----
    inverter_params : inverter2_layout_params
        Parameters for inverter2 sub-generators

    nmos_sw_params : nmos_switch_params
        Parameters for nmos_switch sub-generators

    guard_ring_nf : int
        Width of guard ring

    show_pins : bool
        True to create pin labels
    """

    inverter_params: inverter2_layout_params
    nmos_sw_params: nmos_switch_layout_params
    guard_ring_nf: int
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> mux_sw_layout_params:
        return mux_sw_layout_params(
            inverter_params=inverter2_layout_params.finfet_defaults(min_lch),
            nmos_sw_params=nmos_switch_layout_params.finfet_defaults(min_lch),
            guard_ring_nf=0,
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> mux_sw_layout_params:
        return mux_sw_layout_params(
            inverter_params=inverter2_layout_params.planar_defaults(min_lch),
            nmos_sw_params=nmos_switch_layout_params.planar_defaults(min_lch),
            guard_ring_nf=2,
            show_pins=True,
        )


@dataclass
class mux_sw_params(GeneratorParamsBase):
    layout_parameters: mux_sw_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> mux_sw_params:
        return mux_sw_params(
            layout_parameters=mux_sw_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
