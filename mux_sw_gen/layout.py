from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase

from sal.layout.manual_router import LayerChoice, \
                                     ManualRouter, ManualRouting, UPPER, LOWER, MIDDLE, \
                                     TrackConnection, TrackIntersectionPoint, WirePin
from sal.layout.placement import PlacedInstances

from inverter2_gen.layout import inverter2
from nmos_switch_gen.layout import nmos_switch
from .params import mux_sw_layout_params


class layout(TemplateBase):
    """A 2 to 1 Multiplexer including inverter and NMOS Switch sub-blocks.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='mux_sw_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: mux_sw_layout_params = self.params['params'].copy()
        inv_params = params.inverter_params
        nmos_sw_params = params.nmos_sw_params

        if inv_params.ntap_w != nmos_sw_params.ntap_w or \
           inv_params.ptap_w != nmos_sw_params.ptap_w:
            raise ValueError("Layout generator requires matching ptap_w and ntap_w values to work!")

        # disable pins in subcells
        inv_params.show_pins = False
        nmos_sw_params.show_pins = False

        # get layer IDs
        hm_layer = 4
        vm_layer = hm_layer + 1
        top_layer = vm_layer

        # create layout masters for subcells we will add later
        inv_master = self.new_template(temp_cls=inverter2, params=dict(params=inv_params))
        nmos_sw_master = self.new_template(temp_cls=nmos_switch, params=dict(params=nmos_sw_params))

        # add subcell instances (inv2 and SW1 in first row on bottom, inv1 and SW2 in second row on top)
        inv2_inst = self.add_instance(inv_master, 'inv2', loc=(0, 0), unit_mode=True)
        x1 = inv2_inst.bound_box.right_unit
        SW1_inst = self.add_instance(nmos_sw_master, 'SW1', loc=(x1, 0), unit_mode=True)
        x2 = inv2_inst.bound_box.top_unit
        inv1_inst = self.add_instance(inv_master, 'inv1', loc=(0, x2), unit_mode=True)
        SW2_inst = self.add_instance(nmos_sw_master, 'SW2', loc=(x1, x2), unit_mode=True)

        placed_instances = PlacedInstances(template=self, instances=[inv1_inst, inv2_inst, SW1_inst, SW2_inst])
        placed_instances.set_size_from_bound_box(top_layer_id=top_layer)

        #
        # add wires, vias, pins
        #

        # TrackConnections needed later for add_pin() are assigned to Python variables
        #
        #                                 Net Name /  Net Horizontal Wires / Vertical track intersection point
        track_conn_vout = TrackConnection('OUT',    ['SW1.S', 'SW2.S'],    TrackIntersectionPoint('SW1.S', MIDDLE))

        routing = ManualRouting(
            placed_instances=placed_instances.instances,

            connections_to_tracks=[
                # Net Name              /  Net Horizontal Wires      / Vertical track intersection point
                TrackConnection('ENB',  ['inv1.out', 'inv2.in'],      TrackIntersectionPoint('inv1.out',   UPPER)),
                track_conn_vout,
                TrackConnection('EN',   ['SW1.EN', 'inv2.out'],       TrackIntersectionPoint('inv1.VDD',   UPPER)),
                TrackConnection('ENB',  ['SW2.EN', 'inv1.out'],       TrackIntersectionPoint('inv1.VDD',   UPPER)),
                TrackConnection('VDD',  ['inv1.VDD', 'inv2.VDD'],     TrackIntersectionPoint('inv1.VDD',   LOWER)),
                TrackConnection('VSS',  ['SW1.VSS[0]', 'SW1.VSS[1]',
                                         'inv1.VSS', 'inv2.VSS',
                                         'SW2.VSS[0]', 'SW2.VSS[1]'], TrackIntersectionPoint('SW1.VSS', UPPER)),
            ],

            net_pins={
                'VDD': ['inv1.VDD', 'inv2.VDD'],
                'VSS': ['SW1.VSS[0]', 'SW1.VSS[1]', 'inv1.VSS', 'inv2.VSS', 'SW2.VSS[0]', 'SW2.VSS[1]'],
                'OUT': [WirePin(track_conn_vout, LayerChoice.TOP)]
            },

            reexported_ports=[
                # Net name / Instance pin
                ('CNT',     'inv1.in'),
                ('A',       'SW1.D'),
                ('B',       'SW2.D'),
            ]
        )

        router = ManualRouter(template=self, routing=routing, show_pins=params.show_pins)
        router.route()

        # compute schematic parameters.
        self._sch_params = dict(
            inv_params=inv_master.sch_params,
            nmos_sw_params=nmos_sw_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class mux_sw(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
